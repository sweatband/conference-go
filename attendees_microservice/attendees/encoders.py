from common.json import ModelEncoder
from .models import Attendee, ConferenceVO, AccountVO


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


# work with api_list_attendees
class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]


# work with api_show_attendee
class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceVODetailEncoder(),
    }

    def get_extra_data(self, o):
        count = len(AccountVO.objects.filter(email=o.email))
        if count > 0:
            return {"has_count": True}
        else:
            return {"has_account": False}
