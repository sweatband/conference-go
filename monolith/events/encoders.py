from common.json import ModelEncoder
from .models import Location, Conference


# work with ConferenceDetailEncoder(ModelEncoder):
class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


# work with api_show_location.
# It's a encoder
class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "picture_url",
        "created",
        "updated",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


# work with ConferenceDetailEncoder(ModelEncoder):
class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }
